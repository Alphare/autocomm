# AutoComm #

AutoComm est un script python permettant de commenter automatiquement chaque fichier .css, .php, .html ou .js se trouvant dans le dossier courant ainsi que tous ses sous-dossiers, selon la syntaxe requise par le projet de LIF4.
Bien entendu, avec quelques modifications, il peut tout à fait s'adapter à d'autres extensions, imprimer d'autres formats, etc.. N'hésitez pas à le modifier et à l'utiliser pour tous vos projets.

### Pourquoi ? ###

L'idée m'est venue d'écrire ce script quand j'ai lu dans le sujet qu'il fallait commenter _chacun_ des fichiers du projet. Pourquoi ne pas automatiser le processus ? C'est davantage un challenge qu'autre chose : de manière réaliste, ce n'est pas très utile à notre échelle.

### Comment ça s'installe ? ###

Pour l'instant, AutoComm n'est qu'un script python. Je n'ai pas envisagé de le transformer en éxecutable (pour n'importe quelle plateforme). Peut-être plus tard.  
Il ne marche que sous Linux, avec python 2.7 (je n'ai pas vérifié pour python > 2.7).  

Les étapes pour l'utiliser :

- Téléchargez le dossier, peu importe où.
- Vous y trouverez un fichier settings.py à configurer  
    * Ouvrez-le avec un éditeur de texte
    * Lisez attentivement les commentaires
    * Modifiez à votre guise
- Une fois configuré, ouvrez un terminal :  
    * Déplacez vous dans le répertoire d'AutoComm `cd /chemin/vers/dossier` (rappel : vous pouvez faire TAB pour compléter)
    * Lancez le script `python AutoComm.py /chemin/absolu/vers/le/dossier/`
- Admirez le résultat et réjouissez-vous de ne pas avoir eu besoin de faire des copier-coller laborieux.

### Qu'est-ce qu'il fait exactement ? ###

Le code est disponible et commenté dans AutoComm.py, donc amusez-vous si vous voulez.  
Sinon :  

- Il trouve le dossier donné en argument dans le terminal
- Va faire une copie du dossier dans monDossier_autocomm si `COPY=True` dans `settings.py` (c'est ce qu'il fait par défaut)
- Parcourt le dossier et ses sous-dossier à la recherche de fichiers avec les extensions .css, .php, .html ou .js
- Regarde si le fichier a déjà été commenté grâce à des expressions régulières (multi-lignes, mais avec un "non-greedy" match pour éviter d'effacer un éventuel commentaire présent au début du code). Ne modifiez pas l'entête des commentaires.
- Si oui, il met à jour le commentaire (différent en fonction de l'extension pour que ça compile)
- Si non, il l'ajoute
- Pendant tout ce temps il vous explique tout ce qui se passe (par défaut).

### Arguments optionnels console ###

Vous pouvez passer des arguments optionnels "à la POSIX" :

- `-s` ou `--silent` pour ne pas avoir de log console
- `-c` ou `--clean` efface les commentaires AutoComm

### J'ai une erreur, à l'aide ! ###

Si vous avez une erreur qui n'est pas déjà dans la [liste des erreurs connues](https://bitbucket.org/Alphare/autocomm/issues) ou dans la liste en bas, que vous ne trouvez pas tout seul et que vraiment vous n'arrivez pas à résoudre, envoyez-moi un mail *précis* (c'est-à-dire qui contient le message d'erreur et ce que vous faisiez depuis le début) à `alphare33@gmail.com` avec en début d'objet `[AutoComm]`.  
Si vous avez une recommandation, une idée d'amélioration, etc. à me proposer, vous pouvez la poster sur sur le [tracker](https://bitbucket.org/Alphare/autocomm/issues). N'hésitez pas à préciser votre message et à m'assigner la tâche dans le formulaire.

### Problèmes possibles ###

- Il se peut qu'il vous faille taper `pythonX.Y` (avec X.Y la version de python, pour moi 2.7) au lieu de `python` pour le lancement.
- Il se peut que votre python ne soit pas installé à `/usr/bin/python2.7`. Vous pouvez vérifier avec `whereis python`, ainsi que vérifier votre version avec `python --version`. Si vous trouvez que la version ou l'emplacement est différente, modifiez la première ligne de `AutoComm.py` et `settings.py` en fonction. Je chercherai peut-être une solution plus élégante plus tard.