#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

# Modifier le dossier courant. Si cette option est décochée, une copie du dossier sera faite
#
# Exemple : COPY = True
COPY = False

# Noms (pas chemins !) exacts de dossiers exclus
# Attention, doit être impérativement une liste, même à un seul élément !
#
# Exemple : EXCLUDED_DIRS = ['.git', 'nom_de_dossier']
EXCLUDED_DIRS = ['']

# Nom du projet
# Doit être une chaîne de caractères.
#
# Exemple : PROJECT_NAME = 'Le meilleur projet de LIF4'
PROJECT_NAME = 'Game Sense'

# Nom(s) du/des étudiant(es)
# Attention, doit être impérativement une liste, même à un seul élément !
#
# Exemple : STUDENTS_NAMES = ['Prénom Nom', 'Prénom Nom']
STUDENTS_NAMES = ['Raphaël Gomès']

# Numéro(s) étudiants, dans le même ordre que les noms
# Attention, doit être impérativement une liste, même à un seul élément !
#
# Exemple : STUDENTS_NUMBERS = ['11308829', '12345678']
STUDENTS_NUMBERS = ['11308829']
