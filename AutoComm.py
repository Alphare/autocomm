#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

__author__ = "Raphaël Gomès"
__copyright__ = "Copyright 2015, Raphaël Gomès"
__credits__ = ["Stack Overflow"]
__license__ = "MIT"
__version__ = "0.3.1"
__maintainer__ = "Raphaël Gomès"
__email__ = "alphare33@gmail.com"
__status__ = "Developpement"

# --- Imports statiques
import os
import re
import shutil
import logging
import argparse
from sys import exit

# --- Imports dynamiques

import settings


if __name__ == '__main__':
    # Condition sentinelle. Elle permet au script d'être importé en tant que module sans être éxecuté.

    # --- Parseur d'arguments console
    parser = argparse.ArgumentParser()
    parser.add_argument("-s",
                        "--silent",
                        dest="silent",
                        action="store_true",
                        default=False,
                        help="Le programme n'écrira rien dans la console")

    parser.add_argument("-c",
                        "--clean",
                        dest="clean",
                        action="store_true",
                        default=False,
                        help="Efface les commentaires AutoComm")

    parser.add_argument("-d",
                        "--debug",
                        dest="debug",
                        action="store_true",
                        default=False,
                        help="Affiche les traceback")

    parser.add_argument("path",
                        action="store",
                        help="Le chemin absolu du programme")

    console_args = parser.parse_args()

    # --- Variables
    PATH = console_args.path  # Chemin absolu vers le dossier
    comment_indentifier = "--- AutoComm ---"  # Signature d'AutoComm
    indentation = "--> "  # Indentation dans la console

    # --- Formats
    # Stocker les formats dans un dictionnaire nous permet d'accéder aux valeurs avec une chaîne,
    # ce qui est plus lisible.
    formats = {}
    formats['css_format'] = ("/**\n* " + comment_indentifier +
                             "\n* Nom(s) : {0}\n* "
                             "Numéro(s) étudiant(s) : {1}"
                             "\n* Nom du projet : {2}\n*/\n")
    formats['js_format'] = formats['css_format']
    formats['php_format'] = "<?php\n" + formats['css_format'] + "?>"
    formats['html_format'] = "<!--\n" + formats['css_format'] + "-->"

    # --- Expressions régulières
    # Utiliser des regex pour parser un fichier peut être une relativement mauvaise idée pour quelque chose de plus
    # général, mais pour ça marche bien dans notre cas.
    formats_regex = {}
    formats_regex['css_regex'] = r'/\*\*\n\* ' + comment_indentifier + '.*?\*/\n'
    formats_regex['php_regex'] = r'<\?php\n' + formats_regex['css_regex'] + '\?>'
    formats_regex['html_regex'] = r'<!--\n' + formats_regex['css_regex'] + '-->'



# --- Fonctions
def settingsChecker():
    """
    Vérifie que settings.py est bien configuré.
    Utilise des erreurs personnalisées, parce qu'assert, c'est un peu too much pour un script.
    :return:
    """
    # Si COPY n'est pas un booléen
    if not isinstance(settings.COPY, bool):
        logging.error("Erreur de configuration :\n"
                      +indentation+"COPY n'est pas défini correctement.\n"
                      +indentation+"Veuillez vérifier dans settings.py que COPY est bien un booléen.")
        logging.error("Aucun fichier n'a été modifié.")
        return False

    # Si EXCLUDED_DIRS n'est pas une liste ou que chacun de ses éléments n'est pas une chaîne de caractères
    if not isinstance(settings.EXCLUDED_DIRS, list) \
            and all(isinstance(elem, basestring) for elem in settings.EXCLUDED_DIRS):
        logging.error("Erreur de configuration :\n"
                      +indentation+"EXCLUDED_DIRS n'est pas défini correctement.\n"
                      +indentation+"Veuillez vérifier dans settings.py que EXCLUDED_DIRS est bien une liste.")
        logging.error("Aucun fichier n'a été modifié.")
        return False

    # Si EXCLUDED_DIRS n'est pas une liste ou que chacun de ses éléments n'est pas une chaîne de caractères
    if not isinstance(settings.STUDENTS_NAMES, list) \
            and all(isinstance(elem, basestring) for elem in settings.STUDENTS_NAMES):
        logging.error("Erreur de configuration :\n"
                      +indentation+"STUDENTS_NAMES n'est pas défini correctement.\n"
                      +indentation+"Veuillez vérifier dans settings.py que STUDENTS_NAMES est bien une liste.")
        logging.error("Aucun fichier n'a été modifié.")
        return False

    # Si EXCLUDED_DIRS n'est pas une liste ou que chacun de ses éléments n'est pas une chaîne de caractères
    if not isinstance(settings.STUDENTS_NUMBERS, list) \
            and all(isinstance(elem, basestring) for elem in settings.STUDENTS_NUMBERS):
        logging.error("Erreur de configuration :\n"
                      +indentation+"STUDENTS_NUMBERS n'est pas défini correctement.\n"
                      +indentation+"Veuillez vérifier dans settings.py que STUDENTS_NUMBERS est bien une liste.")
        logging.error("Aucun fichier n'a été modifié.")
        return False

    # Si PROJECT_NAME n'est pas une chaîne de caractères
    if not isinstance(settings.PROJECT_NAME, basestring):
        logging.error("Erreur de configuration :\n"
                      +indentation+"PROJECT_NAME n'est pas défini correctement.\n"
                      +indentation+"Veuillez vérifier dans settings.py que PROJECT_NAME "
                                   "est bien une chaîne de caractères.")
        logging.error("Aucun fichier n'a été modifié.")
        return False

    return True

def copyDirectory():
    """
    Supprime PATH_autocomm s'il existe déjà.
    Fait une copie du dossier (et des sous-dossiers) à traîter vers PATH_autocomm.
    Supprime AutoComm.py du dossier PATH_autocomm s'il se trouve à la racine.
    :return:
    """
    if os.path.isdir(PATH + "_autocomm"):  # Si PATH_autocomm existe
        shutil.rmtree(PATH + "_autocomm")  # On le supprime
        logging.info('Suppression de l\'ancienne version de ' + PATH + "_autocomm.")
    logging.info('Copie du dossier ' + PATH)
    shutil.copytree(PATH, PATH + "_autocomm")  # On copie toute l'arborescence de PATH dans PATH_autocomm
    if os.path.isfile(PATH + "_autocomm/AutoComm.py"):  # Si on trouve AutoComm.py à la racine
        os.remove(PATH + "_autocomm/AutoComm.py")  # On le supprime
    logging.info('Copie réussie vers ' + PATH + '_autocomm\n')
    return

def walkAndCommentDirectory():
    """
    Traverse le dossier et ses sous dossiers et lance les fonctions de traîtement pour chaque fichier.
    :return:
    """
    logging.info('Parcours du dossier et de ses sous-dossiers.')
    logging.info('--------')
    commentStripped = True
    for path, dirs, files in os.walk(PATH):  # On commence à parcourir le dossier
        files = [f for f in files if f.endswith(("php", "html", "css", "js"))]  # On exclut les fichiers non gérés
        dirs[:] = [d for d in dirs if not d[0] == '.']  # On exclut les dossiers cachés
        dirs[:] = [d for d in dirs if d not in settings.EXCLUDED_DIRS]  # On exclut les dossiers EXCLUDED_DIRS
        logging.info("Début du traitement du dossier " + path)
        if not files:
            logging.info(indentation + "Aucun fichier pris en charge dans ce dossier.")
        for filename in files:  # Pour chaque nom de fichier
            file_path = os.path.join(path, filename)  # On trouve son chemin absolu
            extension = getFileExtension(file_path)  # On récupère son extension
            already_commented = checkIfAlreadyCommented(file_path, extension)  # On vérifie s'il a déjà été commenté
            if already_commented is not None:
                logging.info(indentation + 'Fichier déjà commenté. Mise à jour : ' + file_path)
                commentStripped = stripComments(file_path, extension)  # On enlève les commentaires
            # Si l'utilisateur ne fait pas un et qu'il n'y a pas déjà des commentaires
            if console_args.clean is False and commentStripped:
                # On le commente selon le format correspondant
                commentFile(file_path, formats[extension + '_format'], extension)

        logging.info('Fin du traitement du dossier ' + path)
        logging.info('--------')
    return


def getFileExtension(file_path):
    """
    Retourne l'extension (sans le "." devant) du fichier au chemin absolu file_path.
    :param file_path: Chemin absolu du fichier
    :return:
    """
    root, extension = os.path.splitext(file_path)  # On sépare le chemin entre racine et extension
    return extension[1:]  # On renvoie l'extension en enlevant le "." devant.


def commentFile(file_path, format_type, extension):
    """
    Ajoute les commentaires (selon l'extension) au fichier correspondant au chemin file_path.
    :param file_path, le chemin absolu du fichier:
    :return:
    """

    with open(file_path, "r") as original:  # On ouvre le fichier
        old_content = original.read()  # On copie son contenu

    with open(file_path, "w") as modified:  # Sinon on l'ouvre en mode écriture
        modified.seek(0, 0)  # On se place au début du fichier, au cas où l'OS décide de faire le malin

        # On transforme les listes en chaîne de caractères
        students_names = ", ".join(settings.STUDENTS_NAMES)
        students_numbers = ", ".join(settings.STUDENTS_NUMBERS)

        # On formate les variables, et on concatène l'ancien contenu du fichier
        new_content = format_type.format(students_names, students_numbers, settings.PROJECT_NAME) + old_content

        modified.write(new_content)  # On écrit le tout dans le fichier
        logging.info(indentation + 'Commenté ' + file_path)
    return

def checkIfAlreadyCommented(file_path, extension):
    """
    Vérifie si le fichier correspondant au chemin file_path a déjà été traîté par AutoComm.
    :param file_path: Le chemin absolu du fichier
    :param extension: L'extension du fichier
    :return found, booléen:
    """

    found = False
    with open(file_path, "r") as original:
        text = original.read()  # On récupère le contenu
        # Si la recherche de la regex correspondant à l'extension n'est pas nulle
        # DOTALL fait que le caractère "." dans la regex cherche aussi les "\n" et companie
        if re.search(formats_regex[extension+'_regex'], text, flags=re.DOTALL) is not None:
            found = True
    return found


def stripComments(file_path, extension):
    """
    Supprime les commentaires AutoComm existants en fonction de l'extenstion.
    Utilise le nombre de lignes.
    :param file_path: Le chemin absolu du fichier
    :param extension: L'extension du fichier
    :return:
    """
    with open(file_path, "r") as original:  # On ouvre le fichier en lecture
        text = original.read() # On récupère le contenu
        # On supprime la regex correspondant à l'extension dans le texte
        newText = re.sub(formats_regex[extension+'_regex'], "", text, flags=re.DOTALL)

    with open(file_path, 'w') as modified:  # On ouvre le fichier en écriture
        modified.writelines(newText)  # On réécrit le contenu
    return True

def customFunctionAssert(function, *args):
    """
    La fonction assert de base ne permet pas de contrôler le traceback.
    Pour éviter d'afficher des infos de debug à l'utilisateur qui ne les veut pas, j'ai écrit ce wrapper.
    :param function: Le nom de la fonction à évaluer (sans les parenthèses). Doit renvoyer un booléen.
    :param args: Les arguments optionnels de la fonction.
    :return:
    """
    if console_args.debug:
        # Si l'utilisateur veut le traceback, on utilise assert.
        assert function(*args)
    elif not function(*args):
        # Sinon on se contente du message d'erreur dans la fonction et on sort avec 1 pour ne rien afficher.
        exit(1)

# --- Éxecution

if __name__=='__main__':

    # Condition sentinelle. Elle permet au script d'être importé en tant que module sans être éxecuté.

    if console_args.silent:
        # Si l'argument -s ou --silent est passé en console, ne montrer que les logs de niveau ERROR.
        logging.basicConfig(level=logging.ERROR, format='%(message)s')
    else:
        # Sinon ne cacher que le niveau debug
        logging.basicConfig(level=logging.INFO, format='%(message)s')

    logging.info('\n### AutoComm v '+__version__+' ###\n')

    # On vérifie que settings.py est bien initialisé
    customFunctionAssert(settingsChecker)

    if settings.COPY:
        # Faire une copie du dossier de travail et changer PATH à la copie
        copyDirectory()
        PATH += "_autocomm"

    # Parcourir et commenter le dossier
    walkAndCommentDirectory()

    if console_args.clean:
        logging.info('Commentaires effacés avec succès.')
    else:
        logging.info('Dossier commenté avec succès avec les valeurs suivantes :')
        logging.info('Nom(s) : ' + ', '.join(settings.STUDENTS_NAMES))
        logging.info('Numéro(s) : ' + ', '.join(settings.STUDENTS_NUMBERS))
        logging.info('Projet : ' + settings.PROJECT_NAME)